#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
import numpy as np
import tensorflow as tf

def trainRBM(params, data):
    vincks, nh, cd_steps, batch_size, epochs,  = params
    preX = np.random.choice(data, size=(100000, 50))
    min_mag = min(data)
    max_mag = max(data)
    range_mag = max_mag - min_mag
    min_mag -= 0.05*range_mag
    max_mag += 0.05*range_mag
    bins = np.linspace(min_mag, max_mag, 200)
    mags = (bins[1:] + bins[:-1]) / 2
    X = [np.histogram(x, bins=bins)[0] for x in preX]
    X = np.array([x/max(x) for x in X])