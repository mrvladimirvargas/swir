#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
import os
import numpy as np
from pathos.multiprocessing import ProcessingPool as Pool
import tqdm

filenames = os.listdir('results')
filenames.sort(key=lambda x: os.path.getmtime('results/{0}'.format(x)))



def worker_function(fn):
    with open(os.path.join('results', fn), "rb") as f:
        states = pickle.load(f)
    mags = []
    kaps = []
    for estado in states:
        magnetisation = np.count_nonzero(estado[0]==3)/estado[0].size
        mags.append(magnetisation)
        kaps.append(estado[4])
    return mags, kaps

with Pool(64) as pool:
    ms = list(tqdm.tqdm(pool.imap(worker_function, filenames), total=len(filenames)))

#mags = [item[0] for sublist in ms for item in sublist]
#kaps = [item[1] for sublist in ms for item in sublist]
with open('mags.p', "wb") as f:
    pickle.dump(ms, f)