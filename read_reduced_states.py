#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
import os
import numpy as np
from collections import Counter
from scipy.sparse import vstack

howmany = 5000
filenames = os.listdir('results_reduce')
filenames.sort(key=lambda x: os.path.getmtime('results_reduce/{0}'.format(x)))

# A dictionary whose keys will be kappas and whose values will be the number of states stored in
kappas_so_far = {}
tot_kappa = []
tot_mag = []
tot_tim = []
tot_state = []

for fn in filenames[:20]:
    print(fn)
    with open(os.path.join('results_reduce', fn), "rb") as f:
        data = pickle.load(f)
    mags_ = data[0]
    mags_ = [[m]*11 for m in mags_]
    mags_ = np.array([item for sublist in mags_ for item in sublist])
    kappas = data[1]
    kappas = [[k]*11 for k in kappas]
    kappas = np.array([item for sublist in kappas for item in sublist])
    reduced_states = vstack(data[2]).todense()
    tims = data[3]
    tims = [[t]*11 for t in tims]
    tims = np.array([item for sublist in tims for item in sublist])
    c = Counter(kappas)
    for kp in c:
        idx = np.where(kappas==kp)
        thereare = idx[0].size
        if kp in kappas_so_far.keys():
            needed = howmany - kappas_so_far[kp]
            if needed < 1:
                continue
            if thereare > needed:
                tot_state.append(reduced_states[idx][:needed])
                tot_kappa.append(kappas[idx][:needed])
                tot_mag.append(mags_[idx][:needed])
                tot_tim.append(tims[idx][:needed])
                kappas_so_far[kp] += needed
            else:
                tot_state.append(reduced_states[idx])
                tot_kappa.append(kappas[idx])
                tot_mag.append(mags_[idx])
                tot_tim.append(tims[idx])
                kappas_so_far[kp] += thereare
        else:
            needed = howmany
            if thereare > needed:
                tot_state.append(reduced_states[idx][:needed])
                tot_kappa.append(kappas[idx][:needed])
                tot_mag.append(mags_[idx][:needed])
                tot_tim.append(tims[idx][:needed])
                kappas_so_far[kp] = needed
            else:
                tot_state.append(reduced_states[idx])
                tot_kappa.append(kappas[idx])
                tot_mag.append(mags_[idx])
                tot_tim.append(tims[idx])
                kappas_so_far[kp] = thereare

tot_kappa = [item for sublist in tot_kappa for item in sublist]
tot_mag = [item for sublist in tot_mag for item in sublist]
tot_tim = [item for sublist in tot_tim for item in sublist]
tot_state = np.vstack(tot_state)

print(Counter(tot_kappa))
print(len(tot_kappa))
print(len(tot_mag))
print(len(tot_tim))
print(tot_state.shape)

with open("all_states_{0}.p".format(howmany), "wb") as f:
    pickle.dump([tot_kappa, tot_mag, tot_tim, tot_state], f)