#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
from pathos.multiprocessing import ProcessingPool as Pool
import tqdm
from sklearn.preprocessing import OneHotEncoder
from sklearn.random_projection import SparseRandomProjection
from functools import partial
import time
import os
import numpy as np
import time

start_time = time.time()
filenames = os.listdir('results')
filenames.sort(key=lambda x: os.path.getmtime('results/{0}'.format(x)))

def worker_function(estado, encoder, reducer):
    st = time.time()
    magnetisation = np.count_nonzero(estado[0]==3)/estado[0].size
    stationary_state = estado[0]
    stationary_states = [stationary_state]
    for k in range(25):
        new_st = np.random.permutation(stationary_state)
        stationary_states.append(new_st)
    encoded_states = encoder.transform(stationary_states)
    reduced_states = reducer.transform(encoded_states)
    kappa = estado[4]

    return magnetisation, kappa, reduced_states, time.time()-st


# def generate_states(fn, encoder, decoder):
#     with open(os.path.join('results', fn), "rb") as f:
#         states = pickle.load(f)
#     reduced_worker = partial(worker_function, reducer=reducer, encoder=encoder)

#     with Pool(64) as pool:
#         results = list(tqdm.tqdm(pool.imap(reduced_worker, states), total=len(states)))

#     return results

def generate_states(fn, encoder, decoder):
    with open(os.path.join('results', fn), "rb") as f:
        states = pickle.load(f)

    mags = []
    kaps = []
    reds = []
    tims = []

    for estado in tqdm.tqdm(states):
        st = time.time()
        magnetisation = np.count_nonzero(estado[0]==3)/estado[0].size
        stationary_state = estado[0]
        stationary_states = [stationary_state]
        for k in range(10):
            new_st = np.random.permutation(stationary_state)
            stationary_states.append(new_st)
        encoded_states = encoder.transform(stationary_states)
        reduced_states = reducer.transform(encoded_states)
        kappa = estado[4]
        mags.append(magnetisation)
        kaps.append(kappa)
        reds.append(reduced_states)
        tims.append(time.time() - st)

    return mags, kaps, reds, tims



N = 100000
print('Comenzamos')
base_estados = [np.zeros(N, dtype=np.uint8), np.ones(N, dtype=np.uint8)*2, np.ones(N, dtype=np.uint8)*3]
encoder = OneHotEncoder(dtype=np.uint8)
encoder.fit(base_estados)
print('Codificador hecho')
encoded_X = encoder.transform(base_estados)
del base_estados

rng = np.random.RandomState(42)
reducer = SparseRandomProjection(random_state=rng, n_components=500)
reducer.fit(encoded_X)
print('Reductor hecho')
del encoded_X

reduced_worker = partial(worker_function, reducer=reducer, encoder=encoder)

print('Comenzando a reducir')

for idx, fn in enumerate(filenames):
    ms = generate_states(fn, encoder, reducer)
    with open('results_reduce/reduced_many_states_500_{0}.p'.format(idx), "wb") as f:
        pickle.dump(ms, f)
final_time = time.time()


print((final_time - start_time)/60, 'minutos')